using System.Diagnostics;
using Unity.Build.Common;

namespace Unity.Build.Desktop
{
    public abstract class DesktopRunInstance : IRunInstance
    {
        protected bool m_RedirectOutput;
        ProcessOutputHandler m_Stdout;
        ProcessOutputHandler m_Stderr;
        readonly Process m_Process;

        public bool IsRunning => !m_Process.HasExited;

        public void Kill()
        {
            m_Process.Kill();
        }

        protected virtual ProcessStartInfo GetStartInfo(RunContext context)
        {
            return new ProcessStartInfo
            {
                CreateNoWindow = true,
                UseShellExecute = !m_RedirectOutput,
                RedirectStandardOutput = m_RedirectOutput,
                RedirectStandardError = m_RedirectOutput,
            };
        }

        public DesktopRunInstance(RunContext context)
        {
            var settings = context.GetComponentOrDefault<RunSettings>();
            m_RedirectOutput = settings.RedirectOutput;
            m_Stdout = settings.Stdout;
            m_Stderr = settings.Stderr;

            m_Process = new Process()
            {
                StartInfo = GetStartInfo(context),
            };
        }

        public void Dispose()
        {
            m_Process.Dispose();
        }

        protected RunResult Start(RunContext context)
        {
            if (m_RedirectOutput)
            {
                m_Process.OutputDataReceived += (sender, line) =>
                {
                    m_Stdout?.Invoke(sender, line.Data);
                };

                m_Process.ErrorDataReceived += (sender, line) =>
                {
                    m_Stderr?.Invoke(sender, line.Data);
                };
            }

            if (!m_Process.Start())
            {
                return context.Failure($"Failed to start process at '{m_Process.StartInfo.FileName}'.");
            }

            if (m_RedirectOutput)
            {
                m_Process.BeginOutputReadLine();
                m_Process.BeginErrorReadLine();
            }

            return context.Success(this);
        }
    }
}
